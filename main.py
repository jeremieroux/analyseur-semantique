import os
import re
import urllib.request
from bs4 import BeautifulSoup
from interpreteurRegles import *
import itertools

N={} # Dictionnaire de tous les Noeuds (clé:id, valeur:objet Noeud)
N_occ={} # Dictionnaire du nombre d'occurences par nom de Noeud

A={} # Arbre des préfixes

class Noeud:
    id="" # id du mot (mot_occurence)
    idJDM="" # id du mot dans la base Jeux De Mots
    nom="" # texte du mot
    occ=-1 # occurence du mot dans la totalité du texte
    lienS=None # dictionnaire pour tous les liens sortants de ce noeud
    lienE=None # dictionnaire pour tous les liens entrants vers ce noeud
    # Exemple dictionnaire liens {"r_succ": [[noeudSucc, 1],[noeudSucc2, 3]],
    #                             "r_pos": [[noeudNom:, 50], [noeudAdj:, 12]]}
    # r_agent : aboyer r_agent chien
    # r_patient : chien r_patient aboyer
    # r_carac : maison r_carac petite
    # r_lieu :
    # r_manner : Verb -> Adv
    # GN:, GN_DET:, r_head,
    # $x r_agent $y & $y r_head $z -> $x r_agent $z
    # $x==GN: & $x r_succ $y & $y==GV: -> PH:<$x,$y>
    # r_own, r_haspart, r_provider
    # gestion des virgules (si incise, nombre pair de virgules, les virer ou rebrancher successeur)

    def __init__(self, nom): # Constructeur
        self.nom = nom

        # Indexer les doublons
        if (self.nom[0] != ':'):
            if (self.nom in N_occ.keys()):
                N_occ[self.nom] += 1
                self.id = self.nom + "_" + str(N_occ[self.nom])
            else:
                N_occ[self.nom] = 1
                self.id = self.nom
            self.occ = N_occ[self.nom]
        else:
            self.id = self.nom
        N[self.id] = self

    # Affichage du Noeud ainsi que des liens sortants
    def afficher(self):
        if(self.nom[-1]!=':' or (self.nom[0]==':' and self.nom[-1]==':')): # Ne pas afficher les types
            print()
            print("Noeud "+self.id)
            if(self.lienS!=None):
                for r in self.lienS.keys():
                    print("  - "+r)
                    for i in self.lienS[r]:
                        if(i[0]!=None):
                            print("         " + i[0].id +" ("+str(i[1])+")")
                        else:
                            print("         Noeud vide (" + str(i[1]) + ")")
            else:
                print("  Aucun lien sortant")

    # Affichage du Noeud ainsi que des liens sortants
    def afficherSchema(self):
        #if (self.nom[-1] != ':' or (self.nom[0] == ':' and self.nom[-1] == ':')):  # Ne pas afficher les types
            print('"'+self.id+'"')
            if (self.lienS != None):
                for r in self.lienS.keys():
                    for i in self.lienS[r]:
                        if (i[0] != None):
                            print('"'+self.id+'" "'+i[0].id+'" '+r)

    # Renvoie la liste des mots (et poids) impliqués dans des relations sortantes de 'mot' de type nomRel
    def relList(self,nomRel:str):
        r={}
        if(self.nom[0]!=':'):
            source = open(os.getcwd() + '/cacheRel/' + self.nom + '.txt', 'r',encoding="utf-8")
            for line in source:
                elem = line.split(';')
                if(elem[0]==nomRel):
                    r[elem[1]]=int(elem[2])
        return r

    # Vérifie si un mot est dans la liste des relations et a un poids >= val
    def isRelListSup(self, nomRel: str, s: str, val: int):
        r = self.relList(nomRel)
        if ((s in r.keys()) and r[s] >= val):
            return True
        else:
            return False




# Construire un lien dans n de type r vers n2 avec un poids "poids"
def rel(n:Noeud,r:str,n2:Noeud,poids:int):
    if(n.lienS==None): # Lien sortant
        n.lienS={r:[[n2, poids]]}
    elif(r in n.lienS.keys()):
        n.lienS[r].append([n2,poids])
    else:
        n.lienS[r]=[[n2, poids]]

    if (n2 != None):
        if (n2.lienE == None):  # Lien entrant
            n2.lienE = {r: [[n, poids]]}
        elif (r in n2.lienE.keys()):
            n2.lienE[r].append([n, poids])
        else:
            n2.lienE[r] = [[n, poids]]

# Afficher tous les id des noeuds de l'arbre dans l'ordre
def afficherIdNoeuds(n:Noeud):
    while (n != None):
        print(n.id, end=' ')
        n = n.lienS["r_succ"][0][0]
    print()
    print()

# Afficher tous les liens des noeuds de l'arbre dans l'ordre
def afficherLiensNoeuds(n:Noeud):
    while (n != None):
        for r in n.lienS.keys():
            for i in range(len(n.lienS[r])):
                if(n.id[0]!=':' or n.id[-1]!=':') and (n.lienS[r][i][0].id[0]!=':' or n.lienS[r][i][0].id[-1]!=':'):
                    print(n.id + " " + r + "("+str(n.lienS[r][i][1])+") " +n.lienS[r][i][0].id)
        n = n.lienS["r_succ"][0][0]
    print()

# Requête à Jeux de Mots
def JDM(n:Noeud):
    if (n.occ == 1 and not os.path.exists(os.getcwd() + '/cache/' + n.nom + '.txt')):
        print("Téléchargement des informations sur '" + n.nom + "'", end=' ')
        lien = "http://www.jeuxdemots.org/rezo-dump.php?gotermsubmit=Chercher&gotermrel=" + urllib.parse.quote(
            n.nom.encode('utf8'))
        try:
            with open(os.getcwd() + '/cache/' + n.nom + '.txt', 'w',encoding="utf-8") as f:
                go = False
                for line in str(BeautifulSoup(urllib.request.urlopen(lien), 'html.parser')).split('\n'):
                    if (line != "" and line[0] != '/' and line[0] != '<'):
                        if (go):
                            f.write(line + '\n')
                        elif (line.startswith("nt;")):
                            go = True
                            f.write(line + '\n')
            with open(os.getcwd() + '/cacheRel/' + n.nom + '.txt', 'w',encoding="utf-8") as f:
                source = open(os.getcwd() + '/cache/' + n.nom + '.txt', 'r',encoding="utf-8")
                e={}
                rt={}
                moi=None
                for line in source:
                    elem = line.split(';')
                    if(elem[0]=="e"):
                        if(moi==None):
                            moi=elem[1]
                        e[elem[1]]=elem[2][1:-1]
                    elif(elem[0] == "rt"):
                        rt[elem[1]] = elem[2][1:-1]
                    elif(elem[0]=="r" and elem[2]==moi):
                        if(elem[4] in {"4","6","13","14","15","19","34"}):
                            f.write(rt[elem[4]]+";"+e[elem[3]]+";"+elem[5])

        except FileNotFoundError:
            print('Le dossier ' + os.getcwd() + '/cache existe pas')
        print("- OK")
    elif (os.path.exists(os.getcwd() + '/cache/' + n.nom + '.txt')):
        print("Informations sur '" + n.nom + "' déjà dans le cache")

# Obtenir le nom de l'élément sachant son id
def getElem(e:str, nom:str):
    try:
        f = open(os.getcwd() + '/cache/' + nom + '.txt', 'r',encoding="utf-8")
        for line in f:
            elem = line.split(';')
            if(elem[0]=="e" and elem[1]==e):
                return elem[2][1:-1]
        print("ERREUR - Élement non trouvé")
    except FileNotFoundError:
        print('Le dossier ' + os.getcwd() + '/cache existe pas')

# Rediriger les successions (poids négatif sur l'ancienne)
def succession(n1:Noeud,n2:Noeud):
    n1_succ = n1.lienS["r_succ"][-1][0]
    n1_pred = n1.lienE["r_succ"][-1][0]

    n1.lienS["r_succ"][-1][1]=-1
    n1.lienE["r_succ"][-1][1]=-1
    n1_pred.lienS["r_succ"][-1][1]=-1
    n1_succ.lienE["r_succ"][-1][1]=-1

    rel(n1_pred,"r_succ",n2,1)
    rel(n2,"r_succ",n1_succ,1)

# Création des relations pour un mot
def relationDeBase(n:Noeud):
    JDM(n) # Requête à JDM

    # Relations r_pos
    r_pos=n.relList("r_pos")
    for i in r_pos.keys():
        if(i[-1] == ':'):
            rel(n, "r_pos", Noeud(i), r_pos[i])

    # Relations r_lemma
    r_lemma=n.relList("r_lemma")
    if("Ver:" in r_pos.keys() and r_pos["Ver:"]>=0):
        for i in r_lemma.keys():
            if(i!=n.nom):
                t = Noeud(i)
                relationDeBase(t) # Récurrence
                rel(n, "r_lemma", t, r_lemma[i])

# Création de relations depuis les mots de la phrase
def relationDeBaseSucc(n:Noeud):
    os.makedirs("cache", exist_ok=True)  # pour créer le dossier cache
    os.makedirs("cacheRel", exist_ok=True)  # pour créer le dossier cache des relations
    while (n != None):
        relationDeBase(n)
        n = n.lienS["r_succ"][0][0]

def creerSucc(t:[]):
    indexPhrase=0
    go = Noeud(":debut:")
    racine = go

    for phrase in t:
        to = Noeud(":debut" + str(indexPhrase) + ":")
        rel(go, "r_succ", to, 1)
        go = to

        for mot in phrase:
            to = Noeud(mot)
            rel(go, "r_succ", to, 1)
            go = to

        to = Noeud(":fin" + str(indexPhrase) + ":")
        rel(go, "r_succ", to, 1)
        go = to

        indexPhrase+=1

    to = Noeud(":fin:")
    rel(go, "r_succ", to, 1)
    go = to
    rel(go, "r_succ", None, 1)

    return racine

def chargerArbrePrefixe():
    print("Chargement arbre préfixe ",end="")
    try:
        f = open(os.getcwd() + '/motsComposes.txt', 'r',encoding="utf-8")
        for line in f:
            elem = line.split(';')
            terme = elem[1][1:-1]
            mots = terme.lower().split(' ')
            n = A
            for mot in mots:
                if mot not in n.keys():
                    n[mot] = {}
                n = n[mot]
                if (mot == mots[-1]):
                    n[":"] = {}
    except FileNotFoundError:
        print('Le dossier ' + os.getcwd() + 'existe pas')
    print("- OK")

# Revoie True si t est un terme composé d'après l'arbre, False sinon
def estTermeCompose(t:[]):
    sA=A
    for i in t:
        if i in sA.keys():
            sA=sA[i]
        else:
            return False
    if ":" in sA.keys():
        return True
    else:
        return False

# Parcourt tous les termes composés
def termesComposes(nDeb:Noeud):
    nDeb = nDeb.lienS["r_succ"][0][0]
    nFin = None
    while (nDeb!=None and nDeb.lienS["r_succ"][0][0].id[0] != ':'):
        if("Ver:" in nDeb.relList("r_pos") and nDeb.relList("r_lemma") != None): # Cas où il s'agit d'un terme composé commençant par un verbe
            for v_inf in nDeb.relList("r_lemma"): # Pour tous les infinitifs de ce verbe
                s=v_inf
                t=[v_inf]
                nFin = nDeb.lienS["r_succ"][0][0]
                while (nFin.id[0] != ':'):
                    if(s[-1]=="'"):
                        s += nFin.nom
                        t[-1] = t[-1] + nFin.nom
                    else:
                        s += " " + nFin.nom
                        t.append(nFin.nom)

                    if(estTermeCompose(t)):
                        print(s+" - VALIDE")
                        nCom=Noeud(s) # Ajout du nouveau Noeud
                        relationDeBase(nCom)
                        nPred = nDeb.lienE["r_succ"][0][0]
                        nSucc = nFin.lienS["r_succ"][0][0]
                        rel(nPred, "r_succ", nCom, 1) # Branchage du prédécesseur
                        rel(nCom, "r_succ", nSucc, 1) # Branchage du successeur
                        if("Ver:" in nCom.relList("r_pos")):
                            rel(nCom, "r_pos", Noeud("GV:"), nCom.lienS["r_pos"][0][1])
                    else:
                        print(s)
                    nFin = nFin.lienS["r_succ"][0][0]
                nDeb = nDeb.lienS["r_succ"][0][0]
        else:
            s = nDeb.nom
            t = [nDeb.nom]
            nFin = nDeb.lienS["r_succ"][0][0]
            while (nFin.id[0] != ':'):
                if (s[-1] == "'"):
                    s += nFin.nom
                    t[-1] = t[-1] + nFin.nom
                else:
                    s += " " + nFin.nom
                    t.append(nFin.nom)

                if (estTermeCompose(t)):
                    print(s + " - VALIDE")
                    nCom = Noeud(s)  # Ajout du nouveau Noeud
                    relationDeBase(nCom)
                    nPred = nDeb.lienE["r_succ"][0][0]
                    nSucc = nFin.lienS["r_succ"][0][0]
                    rel(nPred, "r_succ", nCom, 1)  # Branchage du prédécesseur
                    rel(nCom, "r_succ", nSucc, 1)  # Branchage du successeur
                    if ("Nom:" in nCom.relList("r_pos")):
                        rel(nCom, "r_pos", Noeud("GN:"), nCom.lienS["r_pos"][0][1])
                else:
                    print(s)
                nFin = nFin.lienS["r_succ"][0][0]
            nDeb = nDeb.lienS["r_succ"][0][0]
    if(nFin.lienS["r_succ"][0][0].lienS["r_succ"][0][0]!=None):
        termesComposes(nFin.lienS["r_succ"][0][0]) # Phrase suivante

def parser(texte:[]):
    t=[]
    for phrase in texte:
        p = phrase.split()
        ph=[]
        for mot in p:
            if "'" in mot: # Prise en compte des apostrophes
                print("OUI",mot)
                mot_ap=mot.split("'")
                ph.append(mot_ap[0]+"'")
                ph.append(mot_ap[1])
            else:
                ph.append(mot)
        t.append(ph)
    return t

def jointure(tuples:[],r:Regle):
    #print(tuples)
    D_v={}
    for id,v_ct in enumerate(r.v_ct):
        D_v[v_ct]=id
    #print(D_v)
    res=[]
    nb_var_ct = len(r.v_ct)
    nb_ct = len(r.ct)
    for elem in list(itertools.product(*tuples)):
        ligne=[None]*nb_var_ct;
        for i in range(nb_ct):
            #print(elem, end=" | ")
            #print("Étape "+str(i))
            # Gauche
            gauche=D_v[r.ct[i].s]
            if(ligne[gauche]==None):
                ligne[gauche]=elem[i][0]
            elif(ligne[gauche]!=elem[i][0]):
                break
            # Droite
            droite=D_v[r.ct[i].o]
            if (ligne[droite] == None):
                ligne[droite] = elem[i][1]
            elif (ligne[droite] != elem[i][1]):
                break
        if(not None in ligne and ligne not in res and len(ligne)==len(set(ligne))):
            res.append(ligne)
    return res

############################### MAIN ###############################

print("-------- OUTIL D'ANALYSE SÉMANTIQUE D'UN TEXTE --------")
print("par Jérémie ROUX, Noha SEOUDI et Yuyang WANG (M2 IA-SD)")
print("=======================================================")
t=parser(re.split(r'[!?;,.\n]', input("\nEntrer le texte :").lower()[:-1]))

#for p in input("\nEntrer le texte :").lower().re.split('.|!|?')[:-1]:
    #t.append(p.split())
print(t)

racine = creerSucc(t)
afficherIdNoeuds(racine)

relationDeBaseSucc(racine)
#afficherLiensNoeuds(racine)

chargerArbrePrefixe()
termesComposes(racine.lienS["r_succ"][0][0])

print()
print(N)
print(N_occ)

# Interpréteur de règles
print("\nRègles :")
chargerRegles()

for id_r,r in enumerate(R):
    print(id_r+1,end=") ")
    if r is None:
        print('La règle est mal formée')
    else:
        r.afficher()
        tuples=[]
        for id_ct,ct in enumerate(r.ct) :
            tuples_i = []
            print("    "+str(id_ct+1),end="] ")
            print(ct.p+" -> ",end="")
            for item in N.values():
                if (item.lienS != None and ct.p in item.lienS.keys()):
                    for i in item.lienS[ct.p]:
                        if (item!=None and i[0]!=None):
                            a = item.nom
                            b = i[0].nom
                            if(ct.s[0]!='$'):
                                a=ct.s
                            if (ct.o[0]!='$'):
                                b = ct.o
                            if(item.nom==a and i[0].nom==b):
                                print("("+item.nom+","+i[0].nom,end=") ")
                                tuples_i.append([item.nom,i[0].nom])
            print("")
            tuples.append(tuples_i)
        print("")
        ress = jointure(tuples,r) # dans l'ordre des variables des contraintes

        gauche = r.v_ct.index(r.cc.s)
        droite = r.v_ct.index(r.cc.o)
        for res in ress:
            rel(N[res[gauche]], r.cc.p, N[res[droite]], 10)
            print(res[gauche] + " " + r.cc.p + " " +res[droite])

for item in N.keys():
    N[item].afficher()

print("https://csacademy.com/app/graph_editor/")
for item in N.keys():
    N[item].afficherSchema()