# Analyseur sémantique du français et extraction de relations à l'aide de JeuxDeMots

## HAI922 - Langage naturel 2 (sémantique des mots et de la phrase)

### Auteurs :

- Noha SEOUDI (M2 IA-SD)
- Jérémie ROUX (M2 IA-SD)
- Yuyang WANG (M2 IA-SD)

## Exécution du programme

Pour lancer le programme, il faut lancer le fichier __main.py__ qui est la racine du code. Il faut alors renseigner la phrase ou les phrases à analyser quand cela est demandé.

## Cache de données

Il y a deux niveaux de cache :

- le dossier __cache__ contient ce que renvoie Jeux de Mots pour chaque mot (en nettoyant les informations inutiles), chaque mot est dans un fichier (exemple : *mot.txt*). Cela permet de ne pas faire d'appel à Jeux de Mots inutilement afin de ne pas surcharger le serveur.
- le dossier __cacheRel__ contient les relations de chaque mot avec traductions des id de Jeux de Mots, chaque mot est dans un fichier (exemple : *mot.txt*). L'information est stockée sous la forme de triplets sur chaque ligne *prédicat/nomRel;objet/noeudSortant;poids*, le nœud entrant (ou *sujet*) étant le nom du fichier dans lequel on se situe.

## Mots composés et arbre préfixe

Le fichier __mots_composes.txt__ contient les mots composés tels que fournis. On utilise ensuite le fichier __arbrePrefixe.txt__ pour former l'arbre préfixe et cet est traduit en texte et stocké par la suite dans le fichier __arbre.txt__. Inutile donc de réexécuter cela sauf s'il y a changement dans le fichier __mots_composes.txt__. À noter que l'arbre préfixe est chargé à chaque exécution depuis le fichier __arbre.txt__ car après de multiples tests, c'était le plus efficace.

## Interpréteur de règles

Le fichier __regles.txt__ contient les règles à interpréter. Toutes ces règles sont utilisées pour enrichir le graphe à la fin de l'exécution du programme, le code de cet interpréteur ce situe dnas le fichier **interpreteurRegles.py**, l'appel à ce fichier est effectué dans **main.py**.