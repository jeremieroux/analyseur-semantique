import os

R = []  # Règles

class Contrainte:
    s = ""  # Sujet (variable)
    p = ""  # Prédicat (nom de la relation)
    o = ""  # Objet (variable)
    positive = True  # Signe de la relation

    # r_action_lieu
    # dans la cuisine
    # dans les choux
    # r_lieu

    def __init__(self, spo):  # Constructeur
        spo = spo.split(" ")
        self.s = spo[0]
        self.o = spo[2]

        if (spo[1][0] == "*"):
            self.positive = False
            self.p = spo[1][1:]
        else:
            self.p = spo[1]

    def afficher(self):
        if (self.positive):
            print("<" + self.s, self.p, self.o + ">", end=" ")
        else:
            print("<" + self.s, "-" + self.p, self.o + ">", end=" ")


class Regle:
    v_ct = None  # Variables des contraintes
    ct = None  # Contraintes
    v_cc = None  # Variables de la conclusion
    cc = None  # Conclusion

    def __init__(self, v_ct, ct, v_cc, cc):  # Constructeur
        self.v_ct = v_ct
        self.ct = []
        for c in ct:
            self.ct.append(Contrainte(c))
        self.v_cc = v_cc
        self.cc = Contrainte(cc)

    def afficher(self):
        for c in self.ct:
            c.afficher()
        print(self.v_ct, end=' ')
        print("->", end=' ')
        self.cc.afficher()
        print(self.v_cc)


def chargerRegles():
    try:
        f = open(os.getcwd() + '/regles.txt', 'r')
        for line in f:
            if (line[-1] == '\n'):
                line = line[:-1]
            elem = line.split(" -> ")
            ct = elem[0].split(" & ")  # Contraintes
            cc = elem[1]  # Conclusion

            # Identification des variables des contraintes
            v_ct = []
            for v in elem[0].split(" "):
                if (v[0] == '$' or v[-1]==':') and v not in v_ct:
                    v_ct.append(v)
                elif v[0] != '$' and v[-1] != ':' and v != "&" and '_' not in v and v not in v_ct: # Pour les constantes
                    v_ct.append(v)

            # Identification des variables de la conclusion
            v_cc = []
            for v in elem[1].split(" "):
                if (v[0] == '$' or v[-1]==':') and v not in v_cc:
                    v_cc.append(v)

            bienForme = True
            for v in v_cc:
                if v not in v_ct:
                    bienForme = False

            if bienForme:
                R.append(Regle(v_ct, ct, v_cc, cc))
            else:
                R.append(None)

    except FileNotFoundError:
        print('Le dossier ' + os.getcwd() + 'existe pas')