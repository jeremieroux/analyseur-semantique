import os

D={}

def ecrire(d,nb:int,f):
    for n in d.keys():
        for i in range(nb):
            f.write(":")
        f.write(n+'\n')
        ecrire(d[n], nb + 1,f)

try:
    f = open(os.getcwd() + '/motsComposes.txt', 'r')
    for line in f:
        elem = line.split(';')
        terme = elem[1][1:-1]
        mots = terme.lower().split(' ')
        n=D
        for mot in mots:
            if mot not in n.keys():
                n[mot] = {}
            n=n[mot]

    #print(D)

    try:
        with open(os.getcwd() + '/arbre.txt', 'w') as f:
            ecrire(D,0,f)
    except FileNotFoundError:
        print('Le dossier ' + os.getcwd() + 'existe pas')

except FileNotFoundError:
    print('Le dossier ' + os.getcwd() + 'existe pas')
