r_isa;ingrédient de cuisine;525
r_isa;aliment;493
r_isa;aliment périssable;394
r_isa;chair animale;386
r_pos;Nom:;361
r_isa;chair;316
r_lemma;viande;300
r_isa;nourriture;223
r_isa;plat;221
r_isa;repas;219
r_lieu;boucherie&g;173
r_isa;ingrédient de recette de cuisine;167
r_pos;Nom:Fem+SG;142
r_lieu;boucherie;123
r_lieu;congélateur;96
r_lieu;abattoir;83
r_isa;produit alimentaire;69
r_lieu;chambre froide;67
r_pos;Gender:Fem;65
r_pos;Number:Sing;65
r_pos;Ver:Conjug;65
r_lieu;cuisine;63
r_lieu;frigo;63
r_lieu;marché;63
r_lieu;cantine;63
r_lieu;plat;63
r_lieu;assiette;63
r_lieu;restaurant;63
r_isa;aliment&g;57
r_pos;Ver:;56
r_lieu;dehors;55
r_lieu;frigidaire;54
r_lemma;viander;54
r_lieu;magasin;53
r_lieu;ferme;53
r_lieu;table;53
r_lieu;supermarché;53
r_lieu;restaurant&g;53
r_pos;Ver:IPre+SG+P1:IPre+SG+P3:SPre+SG+P1:SPre+SG+P3:ImPre+SG+P2;52
r_pos;VerbalMode:Indicatif;52
r_pos;VerbalMode:Subjonctif;46
r_pos;VerbalTime:Present;45
r_lieu;bortsch;43
r_isa;ingrédient;42
r_lieu;boucherie&g;40
r_lieu;panier;38
r_lieu;boucherie chevaline;38
r_lieu;plat de viande;38
r_lieu;plat à four;38
r_lieu;four à vapeur;38
r_lieu;pie&g;38
r_lieu;four;38
r_lieu;hachoir;38
r_lieu;vache;38
r_lieu;réfrigérateur;38
r_lieu;bortch;34
r_lieu;restaurant gastronomique;34
r_lieu;estomac;34
r_isa;préparation culinaire;32
r_lieu;eau;32
r_lieu;marché&g;31
r_lieu;::&g;30
r_lieu;borchtch;30
r_lieu;grande surface;29
r_lieu;restau;29
r_lieu;resto;29
r_isa;bouffe;28
r_lieu;okrochka;28
r_lieu;cuisine&g;28
r_isa;nutriment;26
r_isa;aliment solide;26
r_lieu;plat de service;26
r_pos;VerbalMode:Impératif;25
r_pos;Ver:IPre+SG+P1;25
r_pos;Ver:IPre+SG+P3;25
r_pos;Ver:SPre+SG+P1;25
r_pos;Ver:SPre+SG+P3;25
r_pos;VerbalNumber:SG;25
r_pos;VerbalPers:P1;25
r_pos;VerbalPers:P2;25
r_pos;VerbalPers:P3;25
r_pos;Ver:ImPre+SG+P2;25
r_lieu;en:refrigerator;25
r_lieu;en:freezer;25
r_lieu;en:grill;25
r_lieu;moyenne surface;25
r_lieu;en:supermarket;25
r_lieu;magasin à grande surface;25
r_lieu;en:eatery;25
r_lieu;en:out for a meal;25
r_lieu;ravier;25
r_lieu;en:department store;25
r_lieu;grand magasin;25
r_lieu;en:superstore;25
r_lieu;en:garden centre;25
r_lieu;en:meat course;25
r_lieu;en:serving dish;25
r_lieu;borsch;21
r_lieu;fourchette&g;16
r_lieu;verrine&g;16
r_lieu;grill;16
r_lieu;gril&g;16
r_lieu;restaurant chinois;16
r_lieu;mer;15
r_lieu;mer&g;15
r_lieu;Carrefour;15
r_lieu;en:gourmet restaurant;15
r_lieu;en:fine dining restaurant;15
r_isa;alimentation;13
r_isa;préparation;13
r_lieu;restaurant&g;6
r_lieu;pie;6
r_lieu;gril;6
r_lieu;verrine&g;6
r_lieu;fourchette;6
r_lieu;pizza;2
r_isa;être vivant&g;-13
r_isa;organisme vivant;-13
r_isa;entité vivante;-13
r_isa;être&g;-13
r_isa;organisme animal;-13
r_isa;manger;-18
r_isa;porc;-20
r_isa;lieu&g;-25
r_isa;organisme;-25
r_isa;soupe;-25
r_isa;soupe&g;-25
r_isa;organisme&g;-44
r_isa;animal&g;-46
r_isa;être vivant;-55
r_isa;animal;-67
r_isa;protide;-68
r_isa;eucaryote;-132
r_isa;uniconte;-133
r_isa;opisthoconte;-137
r_isa;holozoaire;-140
r_isa;filozoaire;-144
r_isa;choano-organisme;-152
r_isa;choanobionte;-171
r_isa;métazoaire;-177
r_isa;lieu;-215
r_lemma;viande;300
